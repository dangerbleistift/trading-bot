
from datetime import datetime, timedelta
import grequests
import os
import time
import urllib


from utilities import DT_FORMAT
from utilities import append_line_to_file, get_timestamps_from_lines, get_lines_from_file

BITCOIN_API_URL = 'https://blockchain.info/frombtc?value=100000000&currency=EUR&time='


def get_bitcoin_value_at_datetime(dt: datetime):
    """Retrieves a single bitcoin value at a given datetime time"""
    url = generate_api_url(dt)
    result = fetch_plain_from_url(url)
    # Remove bad characters
    raw_float = result.replace("b", "").replace("'", "").replace(",", "")
    return float(raw_float)

def fetch_plain_from_url(url:str):
    """Fetches a plain text from the given url adress"""
    return str(urllib.request.urlopen(url).read())

def generate_api_url(dt: datetime):
    """Url location of bitcoin value at timestamp"""
    return BITCOIN_API_URL + str(int(dt.timestamp()) * 1000)

def adjust_start(filename: str, start: str, interval: int):
    if os.path.exists(filename):
        print("File {} already existing. Will just update instead of full run".format(filename))
        start = get_timestamps_from_lines(get_lines_from_file(filename))[-1]
        start = (datetime.strptime(start, DT_FORMAT) + timedelta(0, interval)).strftime(DT_FORMAT)
    return start


def fetch_values(start: str, end=datetime.now().strftime(DT_FORMAT), interval=1800):
    """Gets all bitcoin values between two datetime objects, with interval of default 30 minutes"""
    filename = "{}_{}.txt".format(start, interval).replace(":", "_")
    start = adjust_start(filename, start, interval)

    dt_start = datetime.strptime(start, DT_FORMAT)
    dt_end = datetime.strptime(end, DT_FORMAT)

    # build url list
    urls = []
    timestamps = []
    while dt_start <= dt_end:
        urls.append(generate_api_url(dt_start))
        timestamps.append(dt_start.strftime(DT_FORMAT))
        dt_start += timedelta(0, interval)

    # fetch bulk of urls
    chunk_size = 100
    for i in range(0, len(urls), chunk_size):
        chunk_successful = False
        while not chunk_successful:
            try:
                print("Progress: {}/{}".format(i, len(urls)))
                chunk_urls = urls[i:i + chunk_size]
                timestamps_chunk = timestamps[i:i + chunk_size]
                results = grequests.map(grequests.get(u) for u in chunk_urls)
                # merge datasets
                pairs = zip(timestamps_chunk, [r.text for r in results])
                # Parse information
                lines = []
                for pair in pairs:
                    timestamp = pair[0]
                    value = float(pair[1].replace(",", ""))
                    lines.append("{} {}\n".format(timestamp, value))

                # Store information in file
                for line in lines:
                    append_line_to_file(filename, line)

                chunk_successful = True
            except KeyboardInterrupt:
                return 0

            except:
                print("Chunk failed, retrying")
                time.sleep(1)



if __name__ == "__main__":
    fetch_values("2021-01-01_00:00:00")