from itertools import product
import random

DATASET_FILE = "2021-01-01_00_00_00_21600.txt"
CONFIGURATION_LOG = "{}_{}".format(DATASET_FILE.split(".")[0], "configuration.log")
CONFIGURATION_LOG_RANDOM = "{}_{}".format(DATASET_FILE.split(".")[0], "configuration_random.log")


def random_float(min, max):
    return round(random.uniform(min, max) * 10000) / 10000



def random_dcts(length=2000):
    dcts = []
    for i in range(length):
        ts = random_float(0.0001, 0.006)
        hc = int(random_float(10,40))
        tm = random_float(0.005, 0.2)
        tsaf = 1
        bm = random_float(0.01, 0.1)
        bsaf = 1
        bf = 1
        st = "DERIVATIVE"
        
        dct = {
            "ts" : ts,
            "hc" : hc,
            "tm" :tm,
            "tsaf" : tsaf,
            "bm" : bm,
            "bsaf" : bsaf,
            "bf" : bf,
            "st" : st,
        }
        dcts.append(dct)
    
    return dcts

# Simulation Parameters
parameter_dict = {
    "ts" : [0.002, 0.003, 0.004, 0.0045, 0.005],
    "hc" : [13,14,15,16],
    "tm" :[0.05, 0.06, 0.07, 0.08],
    "tsaf" : [6],
    "bm" : [0.05, 0.06, 0.07, 0.08],
    "bsaf" : [1.5, 2, 2.5],
    "bf" : [3, 5, 7],
    "st" : ["HORIZONTAL_SAFE"],
}


def product_dict(input_dict):
    return (dict(zip(input_dict.keys(), values)) for values in product(*input_dict.values()))

PARAMETER_DICTS = list(product_dict(parameter_dict))