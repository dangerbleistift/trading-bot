import urllib.request, json
from collections import defaultdict
import os
from enum import Enum

from utilities import  TIMESTAMP, DIRECTORY
from utilities import avg, fetch_json_from_url, get_values_from_lines, tail, get_lines_from_file, generate_timestamp_value_line, string_get_last_split, append_line_to_file, append_value_to_file


URL_BITCOIN_VALUE = 'https://blockchain.info/ticker'
BOT_CONFIG_FILE = DIRECTORY + '/botconfig.txt'
FILE_BITCOIN_VALUES = DIRECTORY + '/bitcoin_values.txt'
WALLET_EURO_VALUE = DIRECTORY + '/wallet_euro.txt'
WALLET_BITCOIN_VALUE = DIRECTORY + '/wallet_bitcoin.txt'
LOGFILE = DIRECTORY + '/history.log'
# TRADE_SIZE = 0.0005
TAX_RATE_MAKER = 0.0026
TAX_RATE_TAKER = 0.0016
## HORIZONTAL_COUNT = 48*5
## TOP_MARGIN = 0.06
## TOP_SAFETY = 6 * TOP_MARGIN
## BOTTOM_MARGIN = 0.06
## BOTTOM_SAFETY = 2 * BOTTOM_MARGIN
## BIG_FACTOR = 3


# class syntax

# functional syntax
Action = Enum('Action', ['BUY', 'BUY_BIG', 'SELL', ' SELL_BIG','PASS'])
class Action(Enum):
    BUY = 1
    BUY_BIG = 2
    SELL = 3
    SELL_BIG = 4
    PASS = 5
    @classmethod
    def from_string(cls, string):
        if string == "Action.PASS":
            return Action.PASS
        elif string == "Action.BUY":
            return Action.BUY
        elif string == "Action.BUY_BIG":
            return Action.BUY_BIG
        elif string == "Action.SELL":
            return Action.SELL
        elif string == "Action.SELL_BIG":
            return Action.SELL_BIG
        else:
            return None


class Bot():
    def __init__(self, tradesize=0.0005, horizontal_count=48*5, top_margin=0.06, top_safety=6, bottom_margin=0.06, bottom_safety=2, big_factor=3, strategy="SIMPLE", bitcoins=0.05, euros=1000.0):
        self.tradesize = tradesize
        self.horizontal_count = horizontal_count
        self.top_margin = top_margin
        self.top_safety = top_safety
        self.bottom_margin = bottom_margin
        self.bottom_safety = bottom_safety
        self.big_factor = big_factor
        self.strategy = strategy
        self.euro_start = euros
        self.bitcoin_start = bitcoins
        self.buffer = defaultdict(list)
        self.WALLET_BITCOIN_VALUE = WALLET_BITCOIN_VALUE
        self.WALLET_EURO_VALUE = WALLET_EURO_VALUE
        self.FILE_BITCOIN_VALUES = FILE_BITCOIN_VALUES
        self.LOGFILE = LOGFILE
        self.TAX_RATE_MAKER = TAX_RATE_MAKER
        self.TAX_RATE_TAKER = TAX_RATE_TAKER
        self.append_value_to_buffer(self.WALLET_BITCOIN_VALUE, bitcoins)
        self.append_value_to_buffer(self.WALLET_EURO_VALUE, euros)

    def config_to_file(self):
        with open(BOT_CONFIG_FILE, 'w') as f:
            f.write(self.to_config_string())

    def config_from_file(self):        
        if not os.path.exists(BOT_CONFIG_FILE):
            self.config_to_file()
        self.from_config_string(get_lines_from_file(BOT_CONFIG_FILE)[0])


    def buffer_to_file(self):
        for key in self.buffer:
            # print("Writing buffer to {}".format(key))
            with open(key, 'w') as f:
                lines = self.get_last_n_lines_from_buffer(key, 0)
                for line in lines:
                    append_line_to_file(key, line)

    def buffer_from_file(self):
        self.buffer.clear()
        for key in [self.LOGFILE, self.WALLET_BITCOIN_VALUE, self.WALLET_EURO_VALUE, FILE_BITCOIN_VALUES]:
            print("Loading buffer from {}".format(key))
            lines = tail(get_lines_from_file(key), 0)
            for line in lines:
                self.append_line_to_buffer(key, line)

        if self.get_last_n_lines_from_buffer(self.WALLET_BITCOIN_VALUE, 0) == []:
            self.append_value_to_buffer(self.WALLET_BITCOIN_VALUE, 0.05)

        if self.get_last_n_lines_from_buffer(self.WALLET_EURO_VALUE, 0) == []:
            self.append_value_to_buffer(self.WALLET_EURO_VALUE, 1000)

    def strategy_simple(self) -> Action:
        prices = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, 3))
        if (sorted(prices) == prices):
            return Action.BUY
        elif (sorted(prices, reverse=True) == prices):
            return Action.SELL
        else:
            return Action.PASS

    def inverse_action(self, action) -> Action:
        if action == Action.BUY:
            return Action.SELL
        elif action == Action.SELL:
            return Action.BUY
        elif action == Action.BUY_BIG:
            return Action.SELL_BIG
        elif action == Action.SELL_BIG:
            return Action.BUY_BIG
        else:
            return Action.PASS

    def strategy_average(self) -> Action:
        count = 5
        prices = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, count))
        if len(prices) < count:
            return Action.PASS

        average =  avg(prices)
        if average < prices[-1]:
            return Action.BUY
        elif average > prices[-1]:
            return Action.SELL
        else:
            return Action.PASS

    def strategy_tipping(self) -> Action:
        count = 4

        prices = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, 4))
        if len(prices) < count:
            return Action.PASS
        previous = prices[:-1]
        # Previous prices rising
        if (sorted(previous) == previous):
            if prices[-1] < previous[-1]:
                return Action.SELL
            else:
                return Action.BUY

        # Previous prices falling
        elif (sorted(previous, reverse=True) == previous):
            if prices[-1] > previous[-1]:
                return Action.BUY
            else:
                return Action.SELL
        else:
            return Action.PASS

    def strategy_horizontal(self) -> Action:
        count = self.horizontal_count
        prices = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, count))
        if len(prices) < count:
            return Action.PASS

        average =  avg(prices)
        if prices[-1] > average * (1 + self.top_margin):
            return Action.SELL
        elif prices[-1] < average * (1 - self.bottom_margin):
            return Action.BUY
        else:
            return Action.PASS

    def strategy_horizontal_prevent_loss(self) -> Action:
        count = self.horizontal_count
        prices = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, count))
        if len(prices) < count:
            return Action.PASS

        average_current =  avg(prices)
        if prices[-1] < average_current * (1 - self.bottom_safety * self.bottom_margin):
            return Action.SELL_BIG
        elif prices[-1] > average_current and prices[-1] < average_current * (1 + self.top_margin):
            return Action.SELL
        elif prices[-1] > average_current * (1 - self.bottom_margin) and prices[-1] < average_current:
            return Action.BUY
        else:
            return Action.PASS
        
    def strategy_derivative(self) -> Action:
        count = self.horizontal_count + 1
        prices = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, count))
        if len(prices) < count:
            return Action.PASS

        average_current =  avg(prices[1:])
        deriv = average_current - avg(prices[:-1])
        if deriv < 0 and prices[-1] > average_current * (1 + self.top_margin):
            return Action.BUY
        elif deriv > 0 and prices[-1] < average_current * (1 - self.bottom_margin):
            return Action.SELL
        else:
            return Action.PASS


    def try_buy(self) -> bool:
        return self.try_buy_custom(self.tradesize)

    def try_buy_custom(self, amount) -> bool:
        (euros, bitcoins) = self.get_wallet_contents()
        bitcoin_latest_value = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, 1))[0]
        price = bitcoin_latest_value * amount
        has_traded = False
        if (price <= euros):
            self.append_value_to_buffer(self.WALLET_BITCOIN_VALUE, bitcoins + amount * (1 - self.TAX_RATE_TAKER))
            self.append_value_to_buffer(self.WALLET_EURO_VALUE, euros - price)
            has_traded = True

        return has_traded

    def try_sell(self) -> bool:
        return self.try_sell_custom(self.tradesize)

    def try_sell_custom(self, amount) -> bool:
        (euros, bitcoins) = self.get_wallet_contents()
        bitcoin_latest_value = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, 1))[0]
        price = bitcoin_latest_value * amount
        has_traded = False
        if (amount <= bitcoins):
            self.append_value_to_buffer(self.WALLET_BITCOIN_VALUE, bitcoins - amount )
            self.append_value_to_buffer(self.WALLET_EURO_VALUE, euros + price * (1 - self.TAX_RATE_MAKER))
            has_traded = True

        return has_traded

    def bot_action(self, inversed) -> Action:
        action = Action.PASS
        if self.strategy == "AVERAGE":
            action = self.strategy_average()
        elif self.strategy == "TIPPING":
            action = self.strategy_tipping()
        elif self.strategy == "HORIZONTAL":
            action = self.strategy_horizontal()
        elif self.strategy == "HORIZONTAL_SAFE":
            action = self.strategy_horizontal_prevent_loss()
        elif self.strategy == "SIMPLE":
            action = self.strategy_simple()
        elif self.strategy == "DERIVATIVE":
            action = self.strategy_derivative()

        if inversed:
            action = self.inverse_action(action)

        return action


    def get_current_portefolio_value(self):
        (euros, bitcoins) = self.get_wallet_contents()
        try:
            bitcoin_latest_value = get_values_from_lines(self.get_last_n_lines_from_buffer(FILE_BITCOIN_VALUES, 1))[0]
        except IndexError:
            bitcoin_latest_value = -1
        return euros + bitcoins * bitcoin_latest_value


    def get_wallet_contents(self):
        euros = get_values_from_lines(self.get_last_n_lines_from_buffer(self.WALLET_EURO_VALUE, 1))[0]
        bitcoins = get_values_from_lines(self.get_last_n_lines_from_buffer(self.WALLET_BITCOIN_VALUE, 1))[0]
        return (euros, bitcoins)



    def get_last_n_lines_from_buffer(self, key: str, count: int):
        return tail(self.buffer[key], count)

    def append_line_to_buffer(self, key: str, line: str):
        self.buffer[key].append(line)


    def append_value_to_buffer(self, key: str, value: float, timestamp=TIMESTAMP) -> None:
        self.append_line_to_buffer(key, generate_timestamp_value_line(timestamp, value))

    def run(self, timestamp=TIMESTAMP, inversed=False):
        action = self.bot_action(inversed)
        has_traded = False
        if (action == Action.BUY):
            has_traded = self.try_buy()
        elif (action == Action.BUY_BIG):
            has_traded = self.try_buy_custom(self.big_factor * self.tradesize)
        elif (action == Action.SELL):
            has_traded = self.try_sell()
        elif (action == Action.SELL_BIG):
            has_traded = self.try_sell_custom(self.big_factor * self.tradesize)

        summary = self.summarize(action, has_traded)
        self.append_line_to_buffer(self.LOGFILE, "{}\n{}\n{}\n\n".format(timestamp, summary[0], summary[1]))

        return has_traded

    def summarize(self, action: Action, has_traded: bool) -> "tuple[str]":
        (euros, bitcoins) = self.get_wallet_contents()
        bitcoin_latest_value = get_values_from_lines(self.get_last_n_lines_from_buffer(self.FILE_BITCOIN_VALUES, 1))[0]
        portefolio = euros + bitcoins * bitcoin_latest_value
        headline = "({}, {}): {:.2f}€ at {} ({})".format(action, has_traded, portefolio, bitcoin_latest_value, self.strategy)
        description = "Bot tried: {}\nTrade Volume: {}*{}={}\nBot has traded: {}\nPrice (€/BTC): {}\nEuros (€): {}\nBitcoins (BTC): {}\nPortefolio (€): {}".format(action, self.tradesize, bitcoin_latest_value, self.tradesize * bitcoin_latest_value, has_traded, bitcoin_latest_value, euros, bitcoins, portefolio)

        return (headline, description)

    def append_current_bitcoin_value_to_buffer(self) -> float:
        data = fetch_json_from_url(URL_BITCOIN_VALUE)
        self.append_value_to_buffer(self.FILE_BITCOIN_VALUES, data['EUR']['15m'])


    def to_config_string(self):
        configuration = {
            'ev': self.get_current_portefolio_value(),
            'ts': self.tradesize,
            'hc': self.horizontal_count,
            'tm': self.top_margin,
            'tsaf': self.top_safety,
            'bm': self.bottom_margin,
            'bsaf': self.bottom_safety,
            'bf': self.big_factor,
            'st': self.strategy
        }
        return json.dumps(configuration) + "\n"
    
    def from_config_string(self, string):
        data = json.loads(string)
        self.from_config_dict(data)

    def from_config_dict(self, data: dict):
        self.tradesize = data['ts']
        self.horizontal_count = data['hc']
        self.top_margin = data['tm']
        self.top_safety = data['tsaf']
        self.bottom_margin = data['bm']
        self.bottom_safety = data['bsaf']
        self.big_factor = data['bf']
        self.strategy = data['st']



def clean(filelist=[LOGFILE, WALLET_BITCOIN_VALUE, WALLET_EURO_VALUE, FILE_BITCOIN_VALUES]):
    for file in filelist:
        if os.path.isfile(file):
            try:
                os.remove(file)
            except OSError:
                pass



if __name__ == "__main__":

    # init bot
    bot = Bot()
    bot.config_from_file()
    bot.buffer_from_file()
    bot.append_current_bitcoin_value_to_buffer()

    bot.run()
    bot.buffer_to_file()
