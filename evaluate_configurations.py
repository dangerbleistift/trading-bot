from simulator import CONFIGURATION_LOG, simulate_single
from utilities import get_lines_from_file
from plotter import plot
from bot import Bot
import sys
import json

def evaluate(filename, strategy_filter=""):
    lines = get_lines_from_file(filename)
    pairs = []
    for line in lines:
        config_dict = json.loads(line)
        value = config_dict["ev"]
        if strategy_filter == "" or config_dict["st"] == strategy_filter:
            pairs.append((value, config_dict))

    pairs.sort(key=lambda x: x[0])

    for pair in pairs:
        print("{:2f}: {}".format(pair[0], pair[1]))

    return pairs

def plot_best(filename: str, n=10):
    pairs = evaluate(filename)
    for i in range(1, n + 1):
        bot = Bot()
        bot.from_config_dict(pairs[-i][1])
        bot.config_to_file()
        simulate_single(False)
        plot("./plots/{}.png".format(i))


        



if __name__ == "__main__":
    if len(sys.argv) > 1:
        plot_best(sys.argv[1])
    else:
        plot_best(CONFIGURATION_LOG)