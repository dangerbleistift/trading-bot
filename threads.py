from threading import Thread
import multiprocessing
from multiprocessing import Process, Queue
import time
import random
from bot import Action


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args,
                                                **self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return


def fn(i):
    sum = 0
    print("Start ", i)
    if False:
        for (j, i) in enumerate(range(5000000)):

            t1 = time.perf_counter()
            a = Action.from_string("Action.BUY")
            t2 = time.perf_counter()
            sum += (t2 -t1)

        print(sum)

    time.sleep(i)
    print("End", i)
    return i


def fn_wrapper(q:Queue, i):
    q.put(fn(i))


def main_threads():
    count = 10
    lst = [random.randint(1, 4) for i in range(0,count)]
    print(lst)
    threads = []
    for i in range(0, count):
        t = ThreadWithReturnValue(target=fn, args=(lst[i],))
        t.start()
        threads.append(t)

    results = []
    for t in threads:
        results.append(t.join())


    print("All done")
    print(results)

def main_multiprocessing_pool():
    t1 = time.time()
    count = 12
    lst = [random.randint(1, 4) for i in range(0,count)]
    lst = list(range(0, count))
    print(lst)
    pool = multiprocessing.Pool(2)
    results = pool.map(fn, lst)



    print("All done")
    print("Total", time.time() - t1)
    print(results)

def main_multiprocessing_processes():
    count = 10
    lst = [random.randint(1, 4) for i in range(0,count)]
    print(lst)
    processes = []
    queue = Queue()
    for i in range(0, count):
        p = Process(target=fn_wrapper, args=(queue, lst[i],))
        p.start()
        processes.append(p)

    results = []
    for p in processes:
        p.join()

    while queue.qsize() > 0:
        results.append(queue.get())

    print(results)
    print("All done")




if __name__ == "__main__":
    main_multiprocessing_processes()