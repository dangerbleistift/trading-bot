import matplotlib.pyplot as plt
from bot import Action, Bot, LOGFILE
from utilities import string_get_last_split, tail, get_lines_from_file
import os



def plot_averages(ax, dataset: "list[Data]", top_margin, bottom_margin, top_safety, bottom_safety, horizontal_count):
    values = []
    for data in dataset:
        values.append(data.price)

    ax.yaxis.tick_right()
    plt.setp(ax.get_yticklabels(), visible=False)
    alpha=0.7
    ax.plot(averages(values, horizontal_count), linestyle='dotted', color='r')
    ax.plot(list([(1 + top_margin) * x] for x in  averages(values, horizontal_count)), label='Top margin',  color='tab:grey', alpha=alpha)
    ax.plot(list([(1 - bottom_margin) * x] for x in  averages(values, horizontal_count)), label='Bottom margin', color='tab:grey', alpha=alpha)
    ax.plot(list([(1 + top_safety * top_margin) * x] for x in  averages(values, horizontal_count)), label='Top safety', color='k', alpha=alpha)
    ax.plot(list([(1 - bottom_safety * bottom_margin) * x] for x in  averages(values, horizontal_count)), label='Bottom safety', color='k', alpha=alpha)

    ax.legend(loc='upper right', bbox_to_anchor=(1, 1, 0, 0))

def averages(values, horizontal_count):
    count = horizontal_count
    lst = []
    lst = values[0:count]
    for i in range (count, len(values) - 1):
        lst.append(avg(values[i-count:i]))

    return lst


def avg(lst):
    return sum(lst) / len(lst)



def plot_portefolio(ax, dataset: "list[Data]"):
    color = 'tab:blue'
    x = []
    y = []
    for (i, data) in enumerate(dataset):
        x.append(data.datetime.split("_")[0])
        y.append(data.portefolio)

    tick_positions, tick_labels = x_labels(x)

    ax.set_xticks(tick_positions)
    ax.set_ylabel("Portefolio value", color=color)
    ax.tick_params(axis='y', colors=color)
    ax.set_xticklabels(tick_labels, rotation=90)
    ax.plot(y, label='Portefolio (€)', color=color)
    ax.legend(loc='upper right', bbox_to_anchor=(0.8, 1, 0, 0))


def plot_prices(ax, dataset: "list[Data]"):
    color = 'r'
    x = []
    y = []
    for (i, data) in enumerate(dataset):
        x.append(i)
        y.append(data.price)

    ax.yaxis.tick_right()
    ax.set_xticks([])
    ax.set_ylabel("Bitcoin price", color=color)
    ax.tick_params(axis='y', colors=color)
    ax.yaxis.set_label_position('right')
    ax.plot(x, y, color, label='Bitcoin price')
    ax.legend(loc='upper right', bbox_to_anchor=(0.6, 1, 0, 0))


def plot_sales(ax, dataset: "list[Data]"):
    color_buy = 'go'
    color_sell = 'yo'

    x_buy = []
    y_buy = []
    x_sell = []
    y_sell = []
    x_buybig = []
    y_buybig = []
    x_sellbig = []
    y_sellbig = []
    ax.plot
    for (i, data) in enumerate(dataset):
        if data.action == Action.BUY and data.has_traded == True:
            x_buy.append(i)
            y_buy.append(data.price)
        elif data.action == Action.SELL and data.has_traded == True:
            x_sell.append(i)
            y_sell.append(data.price)
        elif data.action == Action.BUY_BIG and data.has_traded == True:
            x_buybig.append(i)
            y_buybig.append(data.price)
        elif data.action == Action.SELL_BIG and data.has_traded == True:
            x_sellbig.append(i)
            y_sellbig.append(data.price)


    ax.yaxis.tick_right()
    plt.setp(ax.get_yticklabels(), visible=False)
    markersize=4
    ax.plot(x_buy, y_buy, 'go', label='Buy', markersize=markersize)
    ax.plot(x_sell, y_sell, 'yo', label='Sell', markersize=markersize)
    ax.plot(x_buybig, y_buybig, 'gD', label='Buy Big', markersize=markersize)
    ax.plot(x_sellbig, y_sellbig, 'yD', label='Sell Big', markersize=markersize)
    ax.legend(loc='upper right', bbox_to_anchor=(0.3, 1, 0, 0))


def plot_euros(ax, dataset: "list[Data]"):
    color = 'tab:blue'
    x = []
    y = []
    for (i, data) in enumerate(dataset):
        x.append(data.datetime.split("_")[0])
        y.append(data.euros)

    tick_positions, tick_labels = x_labels(x)

    ax.yaxis.set_label_position('right')
    ax.yaxis.tick_right()
    ax.set_xticks(tick_positions)
    ax.set_ylabel("Euros", color=color)
    ax.tick_params(axis='y', colors=color)
    ax.set_xticklabels(tick_labels, rotation=90)
    ax.plot(y, label='Bitcoins', linestyle='dashed', color=color)
    ax.legend(loc='upper right', bbox_to_anchor=(0.2, 1, 0, 0))

def plot_bitcoins(ax, dataset: "list[Data]"):
    color = 'tab:grey'
    x = []
    y = []
    for (i, data) in enumerate(dataset):
        x.append(data.datetime.split("_")[0])
        y.append(data.bitcoins)

    tick_positions, tick_labels = x_labels(x)

    ax.set_xticks(tick_positions)
    ax.set_ylabel("Bitcoins", color=color)
    ax.tick_params(axis='y', colors=color)
    ax.set_xticklabels(tick_labels, rotation=90)
    ax.plot(y, label='Euros', linestyle='dotted', color=color)
    ax.legend(loc='upper right', bbox_to_anchor=(0.1, 1, 0, 0))


def x_labels(labels: list):
    no_of_ticks = 15
    tick_positions = range(0, len(labels), int(len(labels) / no_of_ticks) )
    tick_labels = [labels[i] for i in tick_positions]
    return tick_positions, tick_labels

class Data():
    def from_strings(self, strings: "list[str]"):
        self.datetime = string_get_last_split(strings[0])
        self.action = Action.from_string(string_get_last_split(strings[2]))
        self.has_traded = string_get_last_split(strings[4]) == "True"
        self.price = float(string_get_last_split(strings[5]))
        self.euros = float(string_get_last_split(strings[6]))
        self.bitcoins = float(string_get_last_split(strings[7]))
        self.portefolio = float(string_get_last_split(strings[8]))

def plot(filename: str):
    lines = tail(get_lines_from_file(LOGFILE), 0)
    bot = Bot()
    bot.config_from_file()
    logfile_interval = 10
    dataset = []
    for i in range(0, len(lines), logfile_interval):
        data = Data()
        data.from_strings(lines[i: i+ logfile_interval])
        dataset.append(data)
    fig = plt.figure(figsize=(30,15), dpi=200)


    ax_port = fig.add_subplot(211, label="1")
    ax_prices = fig.add_subplot(211, label="2", frame_on=False)
    ax_sales = fig.add_subplot(211, label="3", frame_on=False, sharex=ax_prices, sharey=ax_prices)
    ax_averages = fig.add_subplot(211, label="4", frame_on=False, sharex=ax_prices, sharey=ax_prices)
    ax_euros = fig.add_subplot(212, label="5",)
    ax_bitcoins = fig.add_subplot(212, label="6", frame_on=False,)
    plot_portefolio(ax_port, dataset)
    plot_prices(ax_prices, dataset)
    plot_sales(ax_sales, dataset)
    plot_averages(ax_averages, dataset, bot.top_margin, bot.bottom_margin, bot.top_safety, bot.bottom_safety, bot.horizontal_count)
    plot_euros(ax_euros, dataset)
    plot_bitcoins(ax_bitcoins, dataset)
    plt.show()
    try:
        os.makedirs("/".join(filename.split("/")[:-1]))
    except FileExistsError:
        pass
    plt.savefig(filename)




if "__main__" == __name__:
    plot("./plots/plot.png")

