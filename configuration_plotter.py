import matplotlib.pyplot as plt
import sys
from utilities import get_lines_from_file
from collections import defaultdict

DATA = defaultdict(list)

def plot_configurations(filename: str):
    lines = get_lines_from_file(filename)
    fig = plt.figure(figsize=(30,30), dpi=100)
    for line in lines:
        substrs = line.split(" ")
        for i in range(0, len(substrs), 2):
            key = substrs[i].strip()
            value = to_float_or_default(substrs[i+1].strip())
            DATA[key].append(value)
    
    #print(DATA)
    keys = ['TS', 'HC', 'TM', 'BM', 'BF']
    data = {}
    for key in keys:
        data[key] = (DATA['EV'], DATA[key])
    
    data['BSAF'] = (DATA['EV'], [v1 / v2 for (v1, v2) in zip(DATA['BSAF'], DATA['BM'])])

    for (i, key) in enumerate(data):
        index = len(data) * 100 + 10 + i + 1
        print(index)
        add_plot(fig, index, key, data[key])
    plt.savefig('configurations.png')
    plt.show()

def to_float_or_default(string: str, default=0):
    try:
        return float(string)
    except ValueError:
        return 0


def add_plot(fig, pos, name, data):
    ax = fig.add_subplot(pos, label=name)
    ax.set_xlabel(name)
    ax.plot(data[0], data[1], 'o')





if __name__ == "__main__":
    filename = sys.argv[1]
    plot_configurations(filename)