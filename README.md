# Trading-Bot

This is a simple theoretical trading bot project, that tries to invest in the bitcoin market in a simulation.

Can be attached to an api, if wanted, by yourself :)

## Functionalities

### Full run

You can execute a full run with value fetching and simulations by running 

```
./full_run.sh
```

### Fetch bitcoin values for simulation

For the simulation, you need a dataset of bitcoin values. To generate a dataset, run:

```
python3 value_fetcher.py
```

In the script you can modify the start and end date for the simulation data, aswell as the interval between datapoints.

If you fetch long timeframes, the script might fail because of api reasons. Just restart it after a fail, it picks up where it stopped working

### Trading bot
The Trading bot strategy can be adjusted by defining your own trading decision function and add it to the bot_action() function


### Simulation
Simulations can be done by running

```
python3 simulator.py
```

You can run a single simulation run with simulate() or start a simulation series with your configurations in simulate_config()

Please note that running simulate_config() does not create a trading history for the simulations (for performance reasons).

You can evaluate previous configs by running


```
python3 evaluate_configurations.py
```


### Plotting
Your most recent single simulation can be plotted by running

```
python3 plotter.py
```
