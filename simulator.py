from bot import Bot, clean, FILE_BITCOIN_VALUES
from utilities import get_timestamp_of_line, tail, get_lines_from_file, append_line_to_file
import time
import multiprocessing
import os
import sys
import json
from parameters import *



def simulate(bot: Bot, verbose=False, write_to_file=False):
    clean()
    lines = tail(get_lines_from_file(DATASET_FILE), 0)
    for (i, line) in enumerate(lines):
        if verbose and i % 1000 == 0:
            print(".", end="", flush=True)
        timestamp = get_timestamp_of_line(line)
        bot.append_line_to_buffer(FILE_BITCOIN_VALUES, line)

        bot.run(timestamp)

    if verbose:
        print("")

    portefolio = bot.get_current_portefolio_value()

    configuration = bot.to_config_string()
    if (write_to_file):
        bot.buffer_to_file()
        append_config_to_file(configuration)

    return portefolio, configuration

def append_config_to_file(configuration, filename=CONFIGURATION_LOG):
    append_line_to_file(filename, configuration)


def simulate_single(verbose=True):
    bot = Bot()
    bot.config_from_file()
    simulate(bot, verbose=verbose, write_to_file=True)
    print(bot.to_config_string())

def create_bot_from_parameter_dict(d: dict):
    bot = Bot()
    bot.from_config_dict(d)
    return bot

def get_config_from_config_str(config_str):
    return json.loads(config_str)

def config_in_old_configs(config, old_configs):
    for old in old_configs:
        if config.items() <= old.items():
            return True

    return False

def get_missing_parameter_tpls():
    print("Read existing")
    finished_configurations = get_lines_from_file(CONFIGURATION_LOG)
    old_configs = list(map(get_config_from_config_str, finished_configurations))
    missing = []
    print("Filter existing")
    for d in PARAMETER_DICTS:
        if not config_in_old_configs(d, old_configs):
            missing.append(d)

    return missing


def simulate_multiple_parallel(randomized=False):
    max_value = 0
    max_config = ""
    # lower this if you dont want to use your full cpu during simulation
    processes = os.cpu_count()
    count = 1
    if randomized:
        parameter_tpls = random_dcts()
    else:
        parameter_tpls = get_missing_parameter_tpls()
    total = len(parameter_tpls) // processes
    pool = multiprocessing.Pool(processes=processes)
    for i in range(0, len(parameter_tpls), processes):
        chunk = parameter_tpls[i:i + processes]
        print("Simulation {}/{}".format(count, total))
        t1 = time.time()
        bots = [create_bot_from_parameter_dict(dct) for dct in chunk]
        results = pool.map(simulate, bots)

        for (value, config) in results:
            if (value > max_value):
                max_value = value
                max_config = config

            print("Result:       {}".format(config), end="")
            if randomized:
                append_config_to_file(config, CONFIGURATION_LOG_RANDOM)
            else:
                append_config_to_file(config)


        tdiff = time.time() - t1
        print("Simulation took {} seconds. Ca. {} minutes remaining".format(tdiff, tdiff * (total - count) / 60))

        print("Current best: {}".format(max_config))

        count += 1




if __name__ == "__main__":
    cmd = "parallel"
    if len(sys.argv) > 1:
        cmd = sys.argv[1]

    if cmd == "parallel":
        simulate_multiple_parallel()
    elif cmd == "random":
        simulate_multiple_parallel(True)
    elif cmd == "single":
        simulate_single()




