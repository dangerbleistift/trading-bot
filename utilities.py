from datetime import datetime
import json
import os
import urllib
import sys


DIRECTORY = os.path.dirname(sys.argv[0])
if (DIRECTORY == ""):
    DIRECTORY = "."


DT_FORMAT = '%Y-%m-%d_%H:%M:%S'
TIMESTAMP = datetime.now().strftime(DT_FORMAT)


def string_get_last_split(string: str, separator=" "):
    return string.split(separator)[-1].strip()

def append_value_to_file(filename: str, value: float, timestamp=TIMESTAMP) -> None:
    append_line_to_file(filename, generate_timestamp_value_line(timestamp, value))

def generate_timestamp_value_line(timestamp: str, value: float) -> str:
    return  "{} {}\n".format(timestamp, str(value))

def append_line_to_file(filename: str, line: str) -> None:
    with open(filename, "a") as f:
        f.write(line)

def tail(lst, n):
    return lst[-n:]

def get_lines_from_file(filename: str) -> "list[str]":
    if not os.path.exists(filename):
        print("File {} not existing. Returning empty list".format(filename))
        return []

    with open(filename, 'r') as f:
        return f.readlines()

def get_values_from_lines(lines: "list[str]") -> "list[float]":
    return list(map(get_value_of_line, lines))

def get_timestamps_from_lines(lines: "list[str]") -> "list[float]":
    return list(map(get_timestamp_of_line, lines))


def get_value_of_line(line: str) -> float:
    return float(line.split(" ")[-1])

def get_timestamp_of_line(line: str) -> str:
    return line.split(" ")[0]



# Fetch a json from a url
def fetch_json_from_url(url_adress: str) -> dict:
    with urllib.request.urlopen(url_adress) as url:
        data = json.load(url)
        ## print(data)
        return data





def avg(lst):
    return sum(lst) / len(lst)